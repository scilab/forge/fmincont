// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x,fval,exitflag,output,lambda,grad,hessian] = fmincon_fsqp ( fun,x0,A,b,Aeq,beq,lb,ub,nonlcon,options )
  // Uses fsqp as an implementation of fmincon.
  //
  // Calling Sequence
  //   x = fmincon_fsqp(fun,x0,A,b)
  //   x = fmincon_fsqp(fun,x0,A,b,Aeq,beq)
  //   x = fmincon_fsqp(fun,x0,A,b,Aeq,beq,lb,ub)
  //   x = fmincon_fsqp(fun,x0,A,b,Aeq,beq,lb,ub,nonlcon)
  //   x = fmincon_fsqp(fun,x0,A,b,Aeq,beq,lb,ub,nonlcon,options)
  //   [x,fval,exitflag,output,lambda,grad,hessian] = fmincon_fsqp ( ... )
  //
  // Parameters
  //   fun: the function to minimize
  //   x0: a row vector with dimension n where n is the number of parameters to optimize. Initial guess for optimization algorithm.
  //   options: an optional struct, as provided by optimset
  //
  // Description
  //   Uses fsqp as an implementation of fmincon.
  //
  // Authors
  // Michael Baudin, DIGITEO, 2010

  //===================================================
  // Extract options from data structure
  defaultoptions = optimset ("fmincon");
  MaxFunEvals = optimget ( options , "MaxFunEvals" , "100*numberOfVariables" );
  MaxIter = optimget ( options , "MaxIter" , 400 );
  TolFun = optimget ( options , "TolFun" , defaultoptions.TolFun );
  TolX = optimget ( options , "TolX" , 1.e-6 );
  Display = optimget ( options , "Display" , defaultoptions.Display );
  OutputFcn = optimget ( options , "OutputFcn" , defaultoptions.OutputFcn );
  PlotFcns = optimget ( options , "PlotFcns" , defaultoptions.PlotFcns );
  TolCon = optimget ( options , "TolCon" , defaultoptions.TolCon );
  // If the MaxFunEvals option is a string, this is the default 200 value
  // If not, this is the actual value.
  if ( type ( MaxFunEvals ) == 10 ) then
    if ( MaxFunEvals == "100*numberOfVariables" ) then
      numberofvariables = size(x0,"*")
      MaxFunEvals = 100 * numberofvariables
    else
      errmsg = msprintf(gettext("%s: Unexpected maximum number of function evaluations %s."), "fmincon_fsqp", MaxFunEvals );
      error(errmsg)
    end
  end

  //===================================================
  // Configure options of fsqp from user's options
  nf=1
  modefsqp=100 // TODO : configure this !
  miter = MaxIter
  iprint=1 // TODO : configure this !
  
  bigbnd = %inf
  eps = 1.e-8 // TODO : configure this !
  epsneq=0.e0 // TODO : configure this !
  udelta=0.e0 // TODO : configure this !
  rpar=[bigbnd eps epsneq udelta]
  
  // Compute the number of constraints
  nA = size(A,"r")
  nAeq = size(Aeq,"r")
  if ( nonlcon == [] ) then
    nc = 0
    nceq = 0
  else
    [c, ceq] = nonlcon ( x0 )
    if ( c == [] ) then
      nc = 0
    else
      nc = size(c,"r")
      if ( size(c,"c") <> 1 ) then
        errmsg = msprintf(gettext("%s: Wrong number of columns for the variable c at output of nonlcon: expected 1, but got %d instead."),"fmincon", size(c,"c") );
        error(errmsg);
      end
    end
    if ( ceq == [] ) then
      nceq = 0
    else
      nceq = size(ceq,"r")
      if ( size(ceq,"c") <> 1 ) then
        errmsg = msprintf(gettext("%s: Wrong number of columns for the variable ceq at output of nonlcon: expected 1, but got %d instead."),"fmincon", size(ceq,"c") );
        error(errmsg);
      end
    end
  end
  nineqn = nc
  nineq = nc + nA
  neqn = nceq
  neq = nceq + nAeq
  // Store settings into ipar
  ipar=[nf nineqn nineq neqn neq modefsqp miter iprint]

  
  // Setup default bounds
  if ( lb == [] ) then
    lb = -%inf * ones(nbvar,1)
  end
  if ( ub == [] ) then
    ub = %inf * ones(nbvar,1)
  end

  //===================================================
  // Store data in a global variable
  global fsqp_data
  fsqp_data = tlist(["TFSQP" 
    "fun" 
    "x0"
    "A"
    "b"
    "Aeq"
    "beq"
    "lb"
    "ub"
    "nonlcon"
    "verbose"
    "storeCntr"
    "options"
    ]);
  fsqp_data.fun = fun
  fsqp_data.x0 = x0
  fsqp_data.A = A
  fsqp_data.b = b
  fsqp_data.Aeq = Aeq
  fsqp_data.beq = beq
  fsqp_data.lb = lb
  fsqp_data.ub = ub
  fsqp_data.nonlcon = nonlcon
  fsqp_data.storeCntr = []
  //  fsqp_data.verbose = 1
  fsqp_data.verbose = 0
  fsqp_data.options = options
  if ( %f ) then
    fsqp_data.verbose = 1
  end

  //===================================================
  // Call Fsqp
  [x,inform,fval,g,fslam] = fsqp(x0,ipar,rpar,[lb,ub],fsqp_f, ..
    fsqp_g,"grobfd","grcnfd")
  //===================================================
  // Set output arguments
  //
  // Set exitflag
  // fsqp2fmincon : map from fsqp status to fmincon status
  fsqp2fmincon = [
    0 1
    1 -2
    2 -2
    3 0
    4 %nan
    5 %nan
    6 %nan
    7 %nan
    8 2
    9 %nan
  ];
  // 0 : Number of iterations exceeded options.MaxIter or number of function evaluations exceeded options.FunEvals.
  // 1 : First-order optimality measure was less than options.TolFun, and maximum constraint violation was less than options.TolCon.
  // 2 : Change in x was less than options.TolX and maximum constraint violation was less than options.TolCon.
  // -1 : The output function terminated the algorithm.
  // -2 : No feasible point was found.
  // -3 : Current point x went below options.ObjectiveLimit and maximum constraint violation was less than options.TolCon.
  // %nan : other problem
  // TODO : fmincon = -3

  ist = find ( fsqp2fmincon(:,1) == inform )
  if ( ist <> [] ) then
    // "by chance", the status of fsqp is known
    exitflag = fsqp2fmincon(ist,2)
  else
    exitflag = %nan
  end
  //
  // Set grad
  grad = fsqp_df ( 1 , x )'
  //
  // Set lambda
  lambda = struct(...
      "lower"      , [], ..
      "upper"      , [], ..
      "eqlin"      , [], ..
      "eqnonlin"   , [], ..
      "ineqlin"    , [], ..
      "ineqnonlin" , []);
  for i = 1 : nbvar
    if ( x(i) <> lb(i) ) then
      lambda.lower(i) = 0
    else
      lambda.lower(i) = fslam(i)
    end
    if ( x(i) <> ub(i) ) then
      lambda.upper(i) = 0
    else
      lambda.upper(i) = fslam(i)
    end
  end
  k = nbvar + 1
  if ( nc == 0 ) then
    lambda.ineqnonlin = []
  else
    lambda.ineqnonlin = fslam(k:k+nc-1)
    k = k + nc
  end
  if ( nceq == 0 ) then
    lambda.eqnonlin = []
  else
    lambda.eqnonlin = fslam(k:k+nceq-1)
    k = k + nceq
  end
  if ( nA == 0 ) then
    lambda.ineqlin = []
  else
    lambda.ineqlin = fslam(k:k+nA-1)
    k = k + nA
  end
  if ( nAeq == 0 ) then
    lambda.eqlin = []
  else
    lambda.eqlin = fslam(k:k+nAeq-1)
    k = k + nAeq
  end
  //
  // Set hessian
  fmclambda = [lambda.ineqnonlin' lambda.eqnonlin']'
  hessian = fsqp_hessianLagr ( x , fmclambda )
  //
  // Set output
  output = struct(..
      "iterations"      , [], ..
      "funcCount"       , [], ..
      "stepsize"        , [], ..
      "algorithm"       , [], ..
      "firstorderopt"   , [], ..
      "constrviolation" , [], ..
      "cgiterations" , [], ..
      "message"         , [] );
  output.iterations = [] // TODO
  output.funcCount = [] // TODO
  output.stepsize = []
  output.algorithm = "fsqp"
  output.firstorderopt = [] // TODO
  output.constrviolation = [] // TODO
  output.cgiterations = 0
  output.message = sprintf("%s %s\n%s\n%s", "Optimization terminated:",..
    "first-order optimality relative error", ..
    "less than options.TolFun, and relative constraint violation less", ..
    "than options.TolCon.");
  //
  // Set x
  x = x'
endfunction


// Generates an error if the given variable is not of type real
function apifun_typereal ( var , varname , ivar )
  if ( type ( var ) <> 1 ) then
    errmsg = msprintf(gettext("%s: Expected real variable for variable %s at input #%d, but got %s instead."),"apifun_typereal", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type string
function apifun_typestring ( var , varname , ivar )
  if ( type ( var ) <> 10 ) then
    errmsg = msprintf(gettext("%s: Expected string variable for variable %s at input #%d, but got %s instead."),"apifun_typestring", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type function (macro)
function apifun_typefunction ( var , varname , ivar )
  if ( type ( var ) <> 13 ) then
    errmsg = msprintf(gettext("%s: Expected function but for variable %s at input #%d, got %s instead."),"apifun_typefunction", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type boolean
function apifun_typeboolean ( var , varname , ivar )
  if ( type ( var ) <> 4 ) then
    errmsg = msprintf(gettext("%s: Expected boolean but for variable %s at input #%d, got %s instead."),"apifun_typeboolean", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction

// Generates an error if the given variable has not the right shape
function apifun_shape ( var , varname , ivar , nrows , ncols )
  if ( size(var,"r") <> nrows ) then
    errmsg = msprintf(gettext("%s: Wrong number of rows for variable %s at input #%d: expected %d, but got %d instead."),"apifun_shape", varname , ivar , nrows , size(var,"r") );
    error(errmsg);
  end
  if ( size(var,"c") <> ncols ) then
    errmsg = msprintf(gettext("%s: Wrong number of columns for variable %s at input #%d: expected %d, but got %d instead."),"apifun_shape", varname , ivar , ncols , size(var,"c") );
    error(errmsg);
  end
endfunction

// Generates an error if the value corresponding to an option is unknown.
function apifun_unknownValForOpt ( value , optionname )
      errmsg = msprintf(gettext("%s: Unknown value %s for %s option"),"apifun_unknownValForOpt",value , optionname );
      error(errmsg);
endfunction


//
// fsqp_f --
//   Evaluates the j-th objective function.
function fj = fsqp_f ( j , x )
  global fsqp_data
  fun = fsqp_data.fun
  if ( fsqp_data.verbose == 1 ) then
    mprintf ("fsqp_f\n")
  end
  fj = fun ( x )
endfunction
//
// fsqp_df --
//   Evaluates the gradient of the j-th objective function.
function dfj = fsqp_df ( j , x )
  global fsqp_data
  fun = fsqp_data.fun
  if ( fsqp_data.verbose == 1 ) then
    mprintf ("fsqp_df\n")
  end
  dfj = derivative ( fun , x )
endfunction
//
// fsqp_g --
//   Evaluates the non linear constraints
function gj = fsqp_g ( j , x )
  if ( fsqp_data.verbose == 1 ) then
    mprintf ("fsqp_g\n")
  end
  global fsqp_data
  if ( x_is_new() == 1 ) then
    // Extract data from the problem
    nonlcon = fsqp_data.nonlcon
    A = fsqp_data.A
    b = fsqp_data.b
    Aeq = fsqp_data.Aeq
    beq = fsqp_data.beq
    // Compute non linear inequality and equality constraints
    [c , ceq] = nonlcon ( x )
    // Store constraints
    k = 1
    // Store nonlinear inequality constraints
    nc = size(c,"r")
    fsqp_data.storeCntr(k:k+nc-1) = c
    k = k + nc
    // Store linear inequality constraints
    nA = size(A,"r")
    fsqp_data.storeCntr(k:k+nA-1) = A * x - b
    k = k + nA
    // Store nonlinear equality constraints
    nceq = size(ceq,"r")
    fsqp_data.storeCntr(k:k+nceq-1) = ceq
    k = k + nceq
    // Store linear equality constraints
    nAeq = size(Aeq,"r")
    fsqp_data.storeCntr(k:k+nAeq-1) = Aeq * x - beq
    k = k + nAeq
    set_x_is_new(0)
  end
  gj = fsqp_data.storeCntr(j)
endfunction
//
// fsqp_gFD --
//   Evaluates the non linear constraints for finite differences
function gj = fsqp_gFD ( x , j )
  set_x_is_new(1)
  gj = fsqp_g ( j , x )
endfunction
//
// fsqp_dg --
//   Evaluates the gradient of the non linear constraints
function dg = fsqp_dg ( j , x )
  // TODO
endfunction
//
// fsqp_hessianLagr --
//   Evaluates the Hessian of the objective function
// TODO : take into account for ceq
// TODO : generalize to arbitrary number of constraints
function HL = fsqp_hessianLagr ( x , lambda )
  global fsqp_data
  fun = fsqp_data.fun
  if ( fsqp_data.verbose == 1 ) then
    mprintf ("fsqp_hessianLagr\n")
  end
  [Jf , Hf] = derivative(fun,x,H_form="blockmat" )
  [Jg1 , Hg1] = derivative(list(fsqp_gFD,1),x,H_form="blockmat" )
  [Jg2 , Hg2] = derivative(list(fsqp_gFD,2),x,H_form="blockmat" )
  HL = Hf + lambda(1) * Hg1 + lambda(2) * Hg2
endfunction

