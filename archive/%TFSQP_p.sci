// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//   Prints the string containing the FMINCON data structure.
//
// Calling Sequence
//   %TFSQP_p ( dstr )
//
// Parameters
//  dstr: the FSQP data structure
//
// Description
// Prints the string containing the FSQP data structure.
//
// Authors
// Michael Baudin, DIGITEO, 2010
function %TFSQP_p ( dstr )
  disp(string(dstr))
endfunction

