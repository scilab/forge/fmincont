// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction
function f = objfun ( x )
  f = exp(x(1))*(4*x(1)^2 + 2*x(2)^2 + 4*x(1)*x(2) + 2*x(2) + 1);
endfunction
function [c, ceq] = confun(x)
  // Nonlinear inequality constraints
  c = [
    1.5 + x(1)*x(2) - x(1) - x(2)
    -x(1)*x(2) - 10
  ];
  // Nonlinear equality constraints
  ceq = [];
endfunction
// Make a starting guess at the solution
x0 = [-1,1];
options = optimset("Algorithm","fsqp");
[x,fval,exitflag,output,lambda,grad,hessian] = fmincon ( objfun,x0,[],[],[],[],[],[], confun, options );
xopt = [-9.547405025104247   1.047405025104274];
assert_close ( x , xopt, 1e-4 );
fopt = 0.023550379624176;
assert_close ( fval , fopt , 1e-4 );
assert_equal ( exitflag , 1 );
//assert_equal ( output.iterations > [] , %t ); TODO : FIX THIS !
//assert_equal ( output.funcCount > [] , %t ); TODO : FIX THIS !
//assert_equal ( output.constrviolation <> [] , %t );
//assert_equal ( output.stepsize <> [] , %t ); TODO : FIX THIS !
assert_equal ( output.algorithm , "fsqp" );
//assert_equal ( output.firstorderopt <> [] , %t );
assert_equal ( output.cgiterations , 0 );
assert_equal ( output.message(1) , "Optimization terminated: first-order optimality relative error");
assert_equal ( output.message(2) , "less than options.TolFun, and relative constraint violation less");
assert_equal ( output.message(3) , "than options.TolCon.");
assert_equal ( lambda.eqlin , [] );
assert_equal ( lambda.eqnonlin , [] );
assert_equal ( lambda.ineqlin , [] );
assert_close ( lambda.ineqnonlin , [0.016352744909035 0.018304796364254]' , 1.e-4 );
assert_equal ( lambda.lower , [0 0]' );
assert_equal ( lambda.upper , [0 0]' );
egrad = [
   0.018397030977049
  -0.002284360418892
];
assert_close ( grad , egrad , 1.e-4 );
ehessian = [
   0.014109374550715  -0.007221222310280
  -0.007221222310280   0.044563690583584
];
//assert_close ( hessian , ehessian , 1.e0 ); TODO : FIX THIS !

