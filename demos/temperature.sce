// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of fmincon toolbox
//
// Copyright (C) 2021 - UTC - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function temperature()

function out=cost(u,tf,alpha,_beta)
  X=solution(u,tf,alpha,_beta);
  out=-X(2,$);
end

function [X,t]=solution(u,tf,alpha,_beta)
    function out=rhs(t,X,u,k,alpha,_beta)
        A=X(1);
        k1=u(k);
        k2=alpha*k1^_beta;
        out=[-(k1+k2)*A
        k1*A];
    end
    N=length(u);
    h=tf/N;
    t=0:h:tf;
    X=ode([1;0],0,t(1:2),list(rhs,u,1,alpha,_beta));
    for k=2:length(u)
        X=[X ode(X(:,$),t(k),t(k+1),list(rhs,u,k,alpha,_beta))];
    end
end

R=8.314;
k10=1e6;
k20=5e11;
E1=10000*4.184;// 1 cal=4.184 J, 1 gmol=1 mol
E2=20000*4.184;//
_beta=E2/E1;
alpha=k20/k10^_beta;
Tsup=400; // upper bound for T
k1sup=k10*exp(-E1/R/Tsup); // upper bound for k1

N=15;
tf=1;
h=tf/N;
u=ones(N,1)*k1sup;
lb=zeros(N,1);
ub=u;


clf
subplot(2,1,1)
titlepage("$A\overset{k_1}\longrightarrow B,\quad A\overset{k_2}\longrightarrow C,\quad k_1=k_{10}\exp{\left(-\frac{E_1}{RT}\right)},\quad k_2=k_{20}\exp{\left(-\frac{E_2}{RT}\right)}\\ \begin{array}{rcl}A''&=&-(k_1+k_2)A,\quad A(0)=A_0,\\B''&=&k_1A,\quad B(0)=0,\end{array} \\ \mbox{}\\\operatorname{maximize}B(t_{f}) \mbox{ wih respect to } T(t)\leq 400 \mbox{ piecewise constant on }[0,t_f]$")
gca().box="off"

options = optimoptions('fmincon','display','iter');
u=fmincon(list(cost,tf,alpha,_beta),u,[],[],[],[],lb,ub,[],options);

// Temperature profile
T=E1/R./log(k10./u)

[X,t]=solution(u,tf,alpha,_beta);

subplot(2,2,3)
plot(t,X,t,1-X(1,:)-X(2,:))
legend('A','B','C')
title("Chemical species. B(1) is maximized")
subplot(2,2,4)
h=plot(linspace(0,tf,N+1),[T;T($)])
h.polyline_style=2;
gca().data_bounds(4) = Tsup+1;
legend('T(t)',2)
title("Optimal temperature profile")

demo_viewCode("temperature.sce")

endfunction

temperature()
clear temperature
