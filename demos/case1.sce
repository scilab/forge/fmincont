// Scilab ( http://www.scilab.org/ ) - This file is part of fmincon toolbox
//
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2021 - UTC - Stephane Mottelet
//
// This file must be used under the terms of GPL License
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

////////////////////////////////////////////////////////////////////////
// Definition of the problem
// "Constrained Nonlinear Optimization Examples"
// "Example: Nonlinear Inequality Constraints"
// http://www.mathworks.com/access/helpdesk/help/toolbox/optim/ug/brn4nh7.html
// =============================================================================

function democase1()

mprintf("/////////////////////////////////////////////\n");
mprintf("A simple use case with minimum configuration.\n");

function f = objfun ( x )
  f = exp(x(1))*(4*x(1)^2 + 2*x(2)^2 + 4*x(1)*x(2) + 2*x(2) + 1)
endfunction
function [c, ceq] = confun(x)
  // Nonlinear inequality constraints
  c = [
  1.5 + x(1)*x(2) - x(1) - x(2)
  -x(1)*x(2) - 10
  ]
  // Nonlinear equality constraints
  ceq = []
endfunction

////////////////////////////////////////////////////////////////////////
// The starting point
x0 = [-1,1];
mprintf("\nInitial guess\n");
for i = 1 : 2
  mprintf("x0[%d] = %e\n", i, x0(i));
end
mprintf("The initial guess is not feasible.\n");


////////////////////////////////////////////////////////////////////////
// The expected optimum
// Computed by Matlab
// after 1000 iterations and 3000 function calls
xopt =  [-9.547405025104272   1.047405025104273]
fopt = 0.023550379624175

////////////////////////////////////////////////////////////////////////

// 1. Draw the objective function
function f = objfunC ( x1 , x2 )
  f = objfun ( [x1 x2] )
endfunction

x1 = linspace ( -15 , 10 , 100 );
x2 = linspace ( -10 , 10 , 100 );

clf

contour ( x1 , x2 , objfunC , [0.1 1 10 100 1000] )

// 2. Plot the starting point and the optimum
x0 = [-1,1];
plot ( x0(1) , x0(2) , "bo" )

// 3. Plot the constraints
function g = confun1C ( x1 , x2 ) 
  g = confun ( [x1 x2] )
  g = g(1)
endfunction
//contour ( x1 , x2 , confun2C , [0 10 100] )
contour ( x1 , x2 , confun1C , [0 0] )
mprintf("The contours of f are colored.\n");
mprintf("Lower function values are to the left.\n");

function g = confun2C ( x1 , x2 ) 
  g = confun ( [x1 x2] )
  g = g(2)
endfunction
//contour ( x1 , x2 , confun2C , [0 10 100] )
contour ( x1 , x2 , confun2C , [0 0] )
mprintf("The feasible region is the upper left zone.\n");


// 4. Plot the expected optimum
xopt =  [-9.547345885974547   1.047408305349257]
plot ( xopt(1) , xopt(2) , "r." )

mprintf("Launch fmincon with a display function which\n")
mprintf("plots the intermediate points.\n")

function f = objfunD ( x )
  f = objfun ( x )
endfunction

function out = case1plot(x,optimValues,state)
    if state == "init"
      h = plot(x(1),x(2),"-ro")
      h.tag = "case1"
    else
      h = findobj("tag","case1");
      if state == "iter"
        h.data = [h.data; x(1),x(2)]
      elseif state == "done"
      h.tag = "";
      plot(x(1),x(2),".r")
      end
    end
endfunction

problem = struct();
problem.objective = objfunD;
problem.x0 = x0;
problem.nonlcon = confun;
problem.options = optimoptions("fmincon","Plotfcn",case1plot,"display","iter")

[x,fval,exitflag,output,lambda,grad,hessian] = fmincon(problem)

mprintf("\nSolution of the primal variables, x\n");
for i = 1 : 2
  mprintf("x[%d] = %e\n", i, x(i));
end

mprintf("\nObjective value\n");
mprintf("f(x*) = %e\n", fval);
endfunction

democase1
clear democase1
