// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- NO CHECK REF -->

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
//  if shift < epsilon then
//    flag = 1;
//  else
//    flag = 0;
//  end
//  if flag <> 1 then pause,end
  flag = assert_checktrue ( shift < epsilon );
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
//function flag = assert_equal ( computed , expected )
//  if computed==expected then
//    flag = 1;
//  else
//    flag = 0;
//  end
//  if flag <> 1 then pause,end
//endfunction

//
// Test fmincon / interior-point with a nonlinearily constrained problem.
//

///////////////////////////////////////////////////////////////////////////////////
// Test with gradients

// "Example: Constraints With Gradients"
// http://www.mathworks.com/access/helpdesk/help/toolbox/optim/ug/brn4nh7.html#f11004

// Objective function
function f = objfun ( x )
  f = exp(x(1))*(4*x(1)^2 + 2*x(2)^2 + 4*x(1)*x(2) + 2*x(2) + 1)
endfunction
function [f,G] = objfungrad ( x )
  [lhs,rhs]=argn()
  f = exp(x(1))*(4*x(1)^2+2*x(2)^2+4*x(1)*x(2)+2*x(2)+1)
  if ( lhs  > 1 ) then
    G = [
      f + exp(x(1)) * (8*x(1) + 4*x(2))
      exp(x(1))*(4*x(1)+4*x(2)+2)
    ]
  end
endfunction

// Constraints
function [c, ceq] = confun(x)
  // Nonlinear inequality constraints
  c = [
    1.5 + x(1)*x(2) - x(1) - x(2)
    -x(1)*x(2) - 10
  ]
  // Nonlinear equality constraints
  ceq = []
endfunction
function [c,ceq,DC,DCeq] = confungrad(x)
  // Inequality constraints
  c(1) = 1.5 + x(1) * x(2) - x(1) - x(2)
  c(2) = -x(1) * x(2)-10
  // No nonlinear equality constraints
  ceq=[]
  [lhs,rhs]=argn()
  if ( lhs > 2 ) then
    // DC(:,i) = gradient of the i-th constraint
    // DC = [
    //   Dc1/Dx1  Dc2/Dx1
    //   Dc1/Dx2  Dc2/Dx2
    //   ]
    DC= [
    x(2)-1, -x(2)
    x(1)-1, -x(1)
    ]
    DCeq = []
  end
endfunction

x0 = [-1,1];
[f,G] = objfungrad(x0);
assert_close ( G , [0.3678794  0.7357589]', 1e-4 );
assert_close ( f , 1.8393972, 1e-4 );
[c,ceq,DC,DCeq] = confungrad(x0);
assert_close ( c , [0.5 -9]', 1e-4 );
assert_checkequal ( ceq , [] );
assert_checkequal ( DC , [0,-1;-2,1] );
assert_checkequal ( DCeq , [] );
//
lb = [ ]; 
ub = [ ];   


// With numerical derivatives :
//   iterations: 20
//   funcCount: 21


// Test with gradient of objective only
options = optimoptions("fmincon","SpecifyObjectiveGradient",%t);
[x,fval,exitflag,output] = fmincon(objfungrad,x0,[],[],[],[],lb,ub, confun,options);
assert_close ( x , [-9.547345885974547   1.047408305349257], 1e-4 );
assert_close ( fval , 0.023551460139148 , 1e-4 );
// With exact objective gradient :
//   iterations: 28
//   funcCount: 29

// Test with gradient of constraints only
options = optimoptions("fmincon","SpecifyConstraintGradient",%t);
[x,fval,exitflag,output] = fmincon(objfun,x0,[],[],[],[],lb,ub, confungrad,options);
assert_close ( x , [-9.547345885974547   1.047408305349257], 1e-4 );
assert_close ( fval , 0.023551460139148 , 1e-4 );
// With exact constraints gradient :
//   iterations: 13
//   funcCount: 14

//
// Test with both gradient of objective and gradient of constraints
options = optimoptions("fmincon","SpecifyObjectiveGradient",%t,"SpecifyConstraintGradient",%t);
[x,fval,exitflag,output] = fmincon(objfungrad,x0,[],[],[],[],lb,ub, confungrad,options);
assert_close ( x , [-9.547345885974547   1.047408305349257], 1e-4 );
assert_close ( fval , 0.023551460139148 , 1e-4 );
// With exact objective gradient and exact constraints gradient :
//   iterations: 13
//   funcCount: 14



