// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- NO CHECK REF -->

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
//  if shift < epsilon then
//    flag = 1;
//  else
//    flag = 0;
//  end
//  if flag <> 1 then pause,end
  flag = assert_checktrue ( shift < epsilon );
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
//function flag = assert_equal ( computed , expected )
//  if computed==expected then
//    flag = 1;
//  else
//    flag = 0;
//  end
//  if flag <> 1 then pause,end
//endfunction

//
// Test fmincon / interior-point with a nonlinearily constrained problem.
//
///////////////////////////////////////////////////////////////////////////////////
// Test with bound constraints

function f = objfun ( x )
  f = exp(x(1))*(4*x(1)^2 + 2*x(2)^2 + 4*x(1)*x(2) + 2*x(2) + 1)
endfunction
function [c, ceq] = confun(x)
  // Nonlinear inequality constraints
  c = [
    1.5 + x(1)*x(2) - x(1) - x(2)
    -x(1)*x(2) - 10
  ]
  // Nonlinear equality constraints
  ceq = []
endfunction
// Make a guess at the solution
x0 = [-1,1];
lb = [0,0];
ub = [ ];
[x,fval] = fmincon ( objfun , x0,[],[],[],[],lb,ub,confun);
assert_close ( x , [0   1.5], 1e-4 );
assert_close ( fval , 8.5 , 1e-4 );

