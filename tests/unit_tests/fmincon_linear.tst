// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- NO CHECK REF -->

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

//
// Test fmincon / interior-point with a linearily constrained problem.
//
// http://www.mathworks.com/access/helpdesk/help/toolbox/optim/ug/fmincon.html#f607434
// Find values of x that minimize f(x) = - x1 * x2 * x3, starting at the point x = [10;10;10], 
// subject to the constraints:
// 0 <= x1 + 2x2 + 2x3 <= 72.

function f = objfun ( x )
  f = - prod(x)
endfunction
A = [
  -1 -2 -2
   1  2  2
];
b = [
  0
  72
];

// Initial guess
x0 = [10 10 10]';
[x,fval,exitflag,output,lambda,grad,hessian] = fmincon (objfun,x0,A,b);
assert_close ( x , [24,12,12]', 1e-4 );
assert_close ( fval , -3.456e3 , 1e-4 );
c = A*x-b;
assert_close ( c , [-72;0] , 1e-4 );
// TODO : check exitflag, output, lambda, grad, hessian


