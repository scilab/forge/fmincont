// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// <-- CLI SHELL MODE -->
// <-- NO CHECK REF -->

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
//  if shift < epsilon then
//    flag = 1;
//  else
//    flag = 0;
//  end
//  if flag <> 1 then pause,end
  flag = assert_checktrue ( shift < epsilon );
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
//function flag = assert_equal ( computed , expected )
//  if computed==expected then
//    flag = 1;
//  else
//    flag = 0;
//  end
//  if flag <> 1 then pause,end
//endfunction

//
// Test fmincon / interior-point with a nonlinearily constrained problem.
//
// http://www.mathworks.com/access/helpdesk/help/toolbox/optim/ug/brn4nh7.html#brnox2k
// "Example: Nonlinear Inequality Constraints"

function f = objfun ( x )
  f = exp(x(1))*(4*x(1)^2 + 2*x(2)^2 + 4*x(1)*x(2) + 2*x(2) + 1)
endfunction
function [c, ceq] = confun(x)
  // Nonlinear inequality constraints
  c = [
    1.5 + x(1)*x(2) - x(1) - x(2)
    -x(1)*x(2) - 10
  ]
  // Nonlinear equality constraints
  ceq = []
endfunction
// Make a starting guess at the solution
x0 = [-1,1];
[x,fval,exitflag,output,lambda,grad,hessian] = fmincon ( objfun,x0,[],[],[],[],[],[], confun );
assert_close ( x , [-9.547345885974547   1.047408305349257], 1e-4 );
assert_close ( fval , 0.023551460139148 , 1e-4 );
assert_checkequal ( exitflag , 1 );
assert_checkequal ( output.iterations > 0 , %t );
assert_checkequal ( output.funcCount > 0 , %t );
assert_checkequal ( output.constrviolation <> [] , %t );
//assert_equal ( output.stepsize <> [] , %t ); FIX THIS !
assert_checkequal ( output.algorithm , "ipopt" );
assert_checkequal ( output.firstorderopt <> [] , %t );
//assert_checkequal ( output.cgiterations , 0 );
//assert_checkequal ( output.message(1) , "Optimization terminated: first-order optimality relative error");
//assert_checkequal ( output.message(2) , "less than options.TolFun, and relative constraint violation less");
//assert_checkequal ( output.message(3) , "than options.TolCon.");
assert_checkequal ( lambda.eqlin , [] );
assert_checkequal ( lambda.eqnonlin , [] );
assert_checkequal ( lambda.ineqlin , [] );
assert_close ( lambda.ineqnonlin , [0.016352744909035 0.018304796364254]' , 1.e-4 );
assert_checkequal ( lambda.lower , [0 0]' );
assert_checkequal ( lambda.upper , [0 0]' );
egrad = [
   0.018397841522618
  -0.002284477748397
];
assert_close ( grad , egrad , 1.e-4 );
ehessian = [
   0.013980325480175  -0.006457036550222
  -0.006457036550222   0.044111825516026
];
// The Hessian given by fmincon is computed from a BFGS formula: its accuracy is sometimes poor...
//assert_close ( hessian , ehessian , 1.e0 );


