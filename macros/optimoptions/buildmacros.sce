// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of fmincon toolbox
//
// Copyright (C) 2021 - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

aliases = [
//  setting properties
 "%optim_options_set_finitedifferencestepsize","%optim_options_set_tolerance"
 "%optim_options_set_constrainttolerance","%optim_options_set_tolerance"
 "%optim_options_set_steptolerance","%optim_options_set_tolerance"
 "%optim_options_set_optimalitytolerance","%optim_options_set_tolerance"
 "%optim_options_set_maxfunctionevaluations","%optim_options_set_integer"
 "%optim_options_set_maxiterations","%optim_options_set_integer"
 "%optim_options_set_outputfcn","%optim_options_set_function"
 "%optim_options_set_plotfcn","%optim_options_set_function"
//  fmincon overloads
//  insertion (DON'T FORGET TO ADD NEW TYPES IF APPLICABLE)
 "%b_i_optim_options_fmincon","%i_optim_options"
 "%s_i_optim_options_fmincon","%i_optim_options"
 "%c_i_optim_options_fmincon","%i_optim_options"
 "%sp_i_optim_options_fmincon","%i_optim_options"
 "%function_i_optim_options_fmincon","%i_optim_options"
 "%l_i_optim_options_fmincon","%i_optim_options"
 "%0_i_optim_options_fmincon","%i_optim_options"
//  print, extract
 "%optim_options_fmincon_p","%optim_options_p"
 "%optim_options_fmincon_e","%optim_options_e"
 "%optim_options_fmincon_fieldsdictionary","%optim_options_fieldsdictionary"
 ];

current_dir = pwd();
pathmacros = get_absolute_file_path("buildmacros.sce");
cd(pathmacros)

for i = 1:size(aliases,1)
    orig = aliases(i,2);
    alias = aliases(i,1);
    macroText = mgetl(orig+".sci");
    macroText = strsubst(macroText,orig,alias);
    mputl(macroText,alias+".sci");
end

tbx_build_macros("optimoptions", pathmacros);

for i = 1:size(aliases,1)
   deletefile(aliases(i,1)+".sci");
end

clear tbx_build_macros;
