// Copyright (C) 2020-2023 - UTC - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source w is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function %optim_options_p(opt)
    [st, sthid, solver, empty_content] = %optim_options_defaults(opt)
    if empty_content then
        mprintf("  %s default options:\n",solver);
    else
        mprintf("  - %s user-defined options:\n",solver);
        disp(opt.contents);
        mprintf("\n  - Default options:\n");       
    end
    for f = setdiff(fieldnames(opt.contents),fieldnames(sthid))'
        st(f) = null();
    end
    for f = fieldnames(st)'
        if typeof(st(f)) == "string" 
            st(f) = st(f)(1);
        end
    end
    disp(st)
end