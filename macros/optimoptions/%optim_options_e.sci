// Copyright (C) 2020-2023 - UTC - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source w is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function out = %optim_options_e(i,opt)    
    if typeof(i) == "string" then
        [stdef, sthid, solver, empty_content] = %optim_options_defaults(opt)
        k = find(convstr(fieldnames(stdef)) == convstr(i));
        khid = find(convstr(fieldnames(sthid)) == convstr(i));
        if empty_content == %f
            st = opt.contents;
            ku = find(convstr(fieldnames(st)) == convstr(i));
            if ~isempty(ku)
                out = st(fieldnames(st)(ku));
                return
            end
        end
        // default stuff
        if ~isempty(k)
            out = stdef(fieldnames(stdef)(k));
        elseif ~isempty(khid)
            out = sthid(fieldnames(sthid)(khid));
        else
            error(msprintf("Field ""%s"" does not exists in %s options\n\n",i,solver))
        end
        if typeof(out)=="string"
            out = out(1);
        end
    end
endfunction
