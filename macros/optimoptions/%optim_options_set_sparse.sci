// Copyright (C) 2020-2023 - UTC - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source w is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function out = %optim_options_set_sparse(prop,value,values)
    if or(typeof(value) == ["sparse","boolean sparse"])
        out = value;
    elseif typeof(value) == "constant"
        out = sparse(value);
    else
        error(msprintf("Invalid value for %s option: a sparse matrix is expected.",prop));
    end
end