// Copyright (C) 2020-2023 - UTC - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source w is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function out = %optim_options_set_string(prop,value,values)
    try
        k = find(convstr(value) == convstr(values));
    catch
        k = [];
    end
    if isempty(k) then
        message = msprintf("Invalid value for %s option: valid values are ",prop);
        for i = 1:size(values,"*")
            message = message + msprintf( """%s""",values(i));
            if i < size(values,"*")
                message = message + ","
            end
        end
        error(message)       
    else
        out = values(k);
    end
end