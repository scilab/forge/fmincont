// Copyright (C) 2020-2023 - UTC - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source w is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function out = %optim_options_set_constant(prop,value,values)
    if typeof(value) <> "constant"
        message = msprintf("Invalid value for %s option: a number is expected",prop);
        error(message)       
    elseif floor(values) == values // an integer expected
        if max(0,floor(value)) <> value
            message = msprintf("Invalid value for %s option: a positive integer is expected",prop);
            error(message)                   
        else
            out = value;
        end
    else
        out = value;
    end
end