// Copyright (C) 2020-2023 - UTC - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source w is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function out = %optim_options_set_hessianfcn(prop,value,values)
    if and(typeof(value) <> ["function","list","string"]) then
        message = msprintf("Invalid value for %s option: a function, a list or a string is expected.",prop);
        error(message)       
    else
        if typeof(value) == "string"
            if value <> "objective"
                message = msprintf("Invalid string value for %s option: ""objective"" expected.",prop);
                error(message)
            end
            out = value;
            return
        elseif typeof(value) == "list"
            if size(value) == 0 || typeof(value(1)) <> "function"
                error(msprintf("First element of list must be a function."));
            end
            vars = macrovar(value(1));
        else
            vars = macrovar(value);
        end
        if size(vars(1),"*") < 2 || size(vars(2),"*") < 1
            message = msprintf("Invalid function for %s option: a function with at least 2 input arguments and 1 output argument is expected.",prop);
            error(message)
        end
        out = value;              
    end
end
