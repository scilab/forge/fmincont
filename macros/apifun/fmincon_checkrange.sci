// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function errmsg = fmincon_checkrange ( funname , var , varname , ivar , vmin , vmax )
  // Check that the value is in a given range.
  //
  // Calling Sequence
  //   errmsg = fmincon_checkrange ( funname , var , varname , ivar , expectedopt )
  //
  // Parameters
  //   funname : a 1 x 1 matrix of strings, the name of the calling function.
  //   var : a 1 x 1 matrix of valid Scilab data type, the variable
  //   varname : a 1 x 1 matrix of string, the name of the variable
  //   ivar : a 1 x 1 matrix of floating point integers, the index of the input argument in the calling sequence
  //   vmin : a matrix of values, the minimum value for the variable #ivar
  //   vmax : a matrix of values, the maximum value for the variable #ivar
  //   errmsg : a 1 x 1 matrix of strings, the error message. If there was no error, the error message is the empty matrix.
  //
  // Description
  // This function is designed to be used to design functions where an 
  // input argument is expected to be greater or equal to a threshold.
  // The error is generated if the condition and ( vmin <= var & var <= vmax ) is false.
  // This function can be used for whatever variable type for which 
  // the comparison "<=" can be evaluated.
  //
  // Examples
  // // The function takes an argument x such that 0<= x <=1.
  // function y = myfunction ( x )
  //   fmincon_checkrange ( "myfunction" , x , "x" , 1 , 0 , 1 )
  //   y = sqrt(1-x)
  // endfunction
  // // Calling sequences which work
  // myfunction ( [0.1 0.2 0.8] )
  // // Calling sequences which generate an error
  // myfunction ( [-0.1 0.2 0.8] )
  // myfunction ( [0.1 0.2 1.8] )
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //

  errmsg = []
  if ( ~and ( vmin <= var & var <= vmax ) ) then
    k = find ( vmin > var | var > vmax )
    k = k(1)
    errmsg = msprintf(gettext("%s: Expected that all entries of input argument %s at input #%d are in the range [%s,%s], but entry #%d is equal to %s."),funname,varname,ivar,string(vmin),string(vmax),k,string(var(k)));
    error(errmsg);
  end
endfunction

