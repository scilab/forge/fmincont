// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function errmsg = fmincon_checksquare ( funname , var , varname , ivar )
  // Generates an error if the variable is not a square matrix.
  //
  // Calling Sequence
  //   errmsg = fmincon_checksquare ( funname , var , varname , ivar )
  //
  // Parameters
  //   funname : a 1 x 1 matrix of strings, the name of the calling function.
  //   var : a 1 x 1 matrix of valid Scilab data type, the variable
  //   varname : a 1 x 1 matrix of string, the name of the variable
  //   ivar : a 1 x 1 matrix of floating point integers, the index of the input argument in the calling sequence
  //   errmsg : a 1 x 1 matrix of strings, the error message. If there was no error, the error message is the empty matrix.
  //
  // Description
  // This function is designed to be used to design functions where 
  // the input argument is a square matrix.
  // This function cannot be use when var is a function, or more
  // generally, for any input argument for which the size function
  // does not work.
  //
  // Examples
  // // The function takes a 2 x 3 matrix of doubles.
  // function y = myfunction ( x )
  //   fmincon_checksquare ( "myfunction" , x , "x" , 1 )
  //   y = x
  // endfunction
  // // Calling sequences which work
  // y = myfunction ( ones(2,2) )
  // y = myfunction ( zeros(3,3) )
  // // Calling sequences which generate an error
  // y = myfunction ( ones(1,3) )
  // y = myfunction ( zeros(2,4) )
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //

  errmsg = []
  if ( size(var,"r") <> size(var,"c") ) then
    strcomp = strcat(string(size(var))," ")
    errmsg = msprintf(gettext("%s: Expected a square matrix for input argument %s at input #%d, but got [%s] instead."), funname, varname , ivar , strcomp );
    error(errmsg)
  end
endfunction



