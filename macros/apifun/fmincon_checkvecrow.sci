// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function errmsg = fmincon_checkvecrow ( funname , var , varname , ivar , nbval )
  // Generates an error if the variable is not a row vector.
  //
  // Calling Sequence
  //   errmsg = fmincon_checkvecrow ( funname , var , varname , ivar )
  //
  // Parameters
  //   funname : a 1 x 1 matrix of strings, the name of the calling function.
  //   var : a 1 x 1 matrix of valid Scilab data type, the variable
  //   varname : a 1 x 1 matrix of string, the name of the variable
  //   ivar : a 1 x 1 matrix of floating point integers, the index of the input argument in the calling sequence
  //   nbval : a 1 x 1 matrix of floating point integers, the number of entries in the vector.
  //   errmsg : a 1 x 1 matrix of strings, the error message. If there was no error, the error message is the empty matrix.
  //
  // Description
  // This function is designed to be used to design functions where 
  // the input argument is a row vector, that is, a matrix for which 
  // nrows == 1 and nbcols = nbval.
  // This function cannot be use when var is a function, or more
  // generally, for any input argument for which the size function
  // does not work.
  //
  // Examples
  // // The function takes a row vector of 3 doubles.
  // function y = myfunction ( x )
  //   fmincon_checkvecrow ( "myfunction" , x , "x" , 1 , 3 )
  //   y = x
  // endfunction
  // // Calling sequences which work.
  // y = myfunction ( ones(1,3) )
  // // Calling sequences which generate an error.
  // // This is now a row vector.
  // y = myfunction ( ones(2,3) )
  // // The following have the wrong number of entries.
  // y = myfunction ( ones(1,4) )
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //

  errmsg = []
  nrows = size(var,"r")
  ncols = size(var,"c")
  if ( nrows <> 1 ) then
    errmsg = msprintf(gettext("%s: Expected a row vector for input argument %s at input #%d, but got %d rows instead."), funname, varname , ivar , nrows );
    error(errmsg)
  end
  if ( ncols <> nbval ) then
    errmsg = msprintf(gettext("%s: Expected %d columns for input argument %s at input #%d, but found %d instead."), funname, nbval , varname , ivar , ncols );
    error(errmsg)
  end
endfunction



