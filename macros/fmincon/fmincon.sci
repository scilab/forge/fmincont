// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of fmincon toolbox
//
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2020 - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function [x,fval,exitflag,output,lambda,grad,hessian] = fmincon ( varargin )
  // Solves a nonlinearily constrained optimization problem.
  //
  // Calling Sequence
  //   x = fmincon(fun,x0)
  //   x = fmincon(fun,x0,A,b)
  //   x = fmincon(fun,x0,A,b,Aeq,beq)
  //   x = fmincon(fun,x0,A,b,Aeq,beq,lb,ub)
  //   x = fmincon(fun,x0,A,b,Aeq,beq,lb,ub,nonlcon)
  //   x = fmincon(fun,x0,A,b,Aeq,beq,lb,ub,nonlcon,options)
  //   [x,fval,exitflag,output,lambda,grad,hessian] = fmincon ( ... )
  //
  // Parameters
  //   fun: a function, the function to minimize. See below for the complete specifications.
  //   x0: a nx1 or 1xn matrix of doubles, where n is the number of variables. The initial guess for the optimization algorithm.
  //   A: a nil x n matrix of doubles, where n is the number of variables and nil is the number of linear inequalities. If A==[] and b==[], it is assumed that there is no linear inequality constraints. If (A==[] & b<>[]), fmincon generates an error (the same happens if (A<>[] & b==[])). 
  //   b: a nil x 1 matrix of doubles, where nil is the number of linear inequalities.
  //   Aeq: a nel x n matrix of doubles, where n is the number of variables and nel is the number of linear equalities.  If A==[] and b==[], it is assumed that there is no linear equality constraints. If (Aeq==[] & beq<>[]), fmincon generates an error (the same happens if (Aeq<>[] & beq==[])). 
  //   beq: a nel x 1 matrix of doubles, where nel is the number of linear inequalities.
  //   lb: a nx1 or 1xn matrix of doubles, where n is the number of variables. The lower bound for x. If lb==[], then the lower bound is automatically set to -inf.
  //   ub: a nx1 or 1xn matrix of doubles, where n is the number of variables. The upper bound for x. If lb==[], then the upper bound is automatically set to +inf.
  //   nonlcon: a function, the nonlinear constraints. See below for the complete specifications.
  //   x: a nx1 matrix of doubles, the computed solution of the optimization problem
  //   fval: a 1x1 matrix of doubles, the function value at x
  //   exitflag: a 1x1 matrix of floating point integers, the exit status. See below for details.
  //   output: a struct, the details of the optimization process.  See below for details.
  //   lambda: a struct, the Lagrange multipliers at optimum.  See below for details.
  //   grad: a nx1 matrix of doubles, the gradient of the objective function at optimum
  //   hessian: a nxn matrix of doubles, the Hessian of the objective function at optimum
  //   options: an optional list, as provided by optimoptions
  //
  // Description
  // Search the minimum of a constrained optimization problem specified by :
  // find the minimum of f(x) such that 
  //
  //<latex>
  //\begin{eqnarray}
  //\mbox{min}_{x}    & f(x) \\
  //\mbox{subject to} & c(x) \leq 0 \\
  //                  & c_{eq}(x) = 0 \\
  //                  & Ax \leq b \\
  //                  & A_{eq} x = b_{eq} \\
  //                  & lb \leq x \leq ub
  //\end{eqnarray}
  //</latex>
  //
  // Currently, we use ipopt for the actual solver of fmincon.
  //
  // See the demonstrations for additionnal examples.
  //
  // The objective function must have header :
  // <programlisting>
  //   f = objfun ( x )
  // </programlisting>
  // where x is a n x 1 matrix of doubles and f is a 1 x 1 matrix of doubles.
  // On input, the variable x contains the current point and, on output, 
  // the variable f must contain the objective function value.
  //
  // By default, fmincon uses finite differences with order 2 formulas and 
  // optimum step size in order to compute a numerical gradient of the 
  // objective function.
  // If we can provide exact gradients, we should do so since it improves 
  // the convergence speed of the optimization algorithm.
  // In order to use exact gradients, we must update the header of the 
  // objective function to :
  // <programlisting>
  //   [f,G] = objfungrad ( x )
  // </programlisting>
  // where x is a n x 1 matrix of doubles, f is a 1 x 1 matrix of doubles
  // and G is a n x 1 matrix of doubles.
  // On input, the variable x contains the current point and, on output, 
  // the variable f must contain the objective function value and the variable 
  // G must contain the gradient of the objective function.
  // Furthermore, we must enable the "GradObj" option with the statement :
  // <programlisting>
  //   options = optimset("GradObj","on");
  // </programlisting>
  // This will let fmincon know that the exact gradient of the objective 
  // function is known, so that it can change the calling sequence to the 
  // objective function.
  //
  // The constraint function must have header :
  // <programlisting>
  //   [c, ceq] = confun(x)
  // </programlisting>
  // where x is a n x 1 matrix of doubles, c is a nni x 1 matrix of doubles and 
  // ceq is a nne x 1 matrix of doubles (nni : number of nonlinear inequality 
  // constraints, nne : number of nonlinear equality constraints).
  // On input, the variable x contains the current point and, on output, 
  // the variable c must contain the nonlinear inequality constraints and ceq must contain the 
  // nonlinear equality constraints.
  //
  // By default, fmincon uses finite differences with order 2 formulas and
  // optimum step size in order to compute a numerical gradient of the 
  // constraint function.
  // In order to use exact gradients, we must update the header of the 
  // constraint function to :
  // <programlisting>
  //   [c,ceq,DC,DCeq] = confungrad(x)
  // </programlisting>
  // where x is a n x 1 matrix of doubles, c is a nni x 1 matrix of doubles, 
  // ceq is a nne x 1 matrix of doubles, DC is a n x nni matrix of doubles and 
  // DCeq is a n x nne matrix of doubles.
  // On input, the variable x contains the current point and, on output, 
  // the variable c must contain the nonlinear inequality constraint function value,
  // the variable ceq must contain the nonlinear equality constraint function value,
  // the variable DC must contain the Jacobian matrix of the nonlinear inequality constraints
  // and the variable DCeq must contain the Jacobian matrix of the nonlinear equality constraints.
  // The i-th nonlinear inequality constraint is associated to the i-th column of 
  // the matrix DC, i.e, it is stored in DC(:,i) (same for DCeq).
  // Furthermore, we must enable the "GradObj" option with the statement :
  // <programlisting>
  //   options = optimset("GradConstr","on");
  // </programlisting>
  //
  // By default, fmincon uses a L-BFGS formula to compute an 
  // approximation of the Hessian of the Lagrangian.
  // Notice that this is different from Matlab's fmincon, which 
  // default is to use a BFGS.
  //
  // The exitflag variable allows to know the status of the optimization.
  // <itemizedlist>
  //   <listitem>exitflag=0 : Number of iterations exceeded options.MaxIter or number of function evaluations exceeded options.FunEvals.</listitem>
  //   <listitem>exitflag=1 : First-order optimality measure was less than options.TolFun, and maximum constraint violation was less than options.TolCon.</listitem>
  //   <listitem>exitflag=-1 : The output function terminated the algorithm.</listitem>
  //   <listitem>exitflag=-2 : No feasible point was found.</listitem>
  //   <listitem>exitflag=%nan : Other type of termination.</listitem>
  // </itemizedlist>
  // TODO : 2 : Change in x was less than options.TolX and maximum constraint violation was less than options.TolCon. 
  // TODO : -3 : Current point x went below options.ObjectiveLimit and maximum constraint violation was less than options.TolCon. 
  //
  // The output data structure contains detailed informations about the 
  // optimization process. 
  // It has type "struct" and contains the following fields.
  // <itemizedlist>
  //   <listitem>output.iterations: the number of iterations performed during the search</listitem>
  //   <listitem>output.funcCount: the number of function evaluations during the search</listitem>
  //   <listitem>output.stepsize: an empty matrix</listitem>
  //   <listitem>output.algorithm : the string containing the name of the algorithm. In the current version, algorithm="ipopt".</listitem>
  //   <listitem>output.firstorderopt: the max-norm of the first-order KKT conditions.</listitem>
  //   <listitem>output.constrviolation: the max-norm of the constraint violation.</listitem>
  //   <listitem>output.cgiterations: the number of preconditionned conjugate gradient steps. In the current version, cgiterations=0.</listitem>
  //   <listitem>output.message: a string containing a message describing the status of the optimization.</listitem>
  // </itemizedlist>
  //
  // The lambda data structure contains the Lagrange multipliers at the 
  // end of optimization.
  // It has type "struct" and contains the following 
  // fields.
  // <itemizedlist>
  //   <listitem>lambda.lower: the Lagrange multipliers for the lower bound constraints.</listitem>
  //   <listitem>lambda.upper: the Lagrange multipliers for the upper bound constraints.</listitem>
  //   <listitem>lambda.eqlin: the Lagrange multipliers for the linear equality constraints.</listitem>
  //   <listitem>lambda.eqnonlin: the Lagrange multipliers for the nonlinear equality constraints.</listitem>
  //   <listitem>lambda.ineqlin: the Lagrange multipliers for the linear inequality constraints.</listitem>
  //   <listitem>lambda.ineqnonlin: the Lagrange multipliers for the nonlinear inequality constraints.</listitem>
  // </itemizedlist>
  //
  // TODO : exitflag=-3 : Current point x went below options.ObjectiveLimit and maximum constraint violation was less than options.TolCon. 
  // TODO : implement MaxFunEvals option
  // TODO : implement DerivativeCheck option
  // TODO : implement TolFun option
  // TODO : implement TolX option
  // TODO : implement Hessian option
  // TODO : check that the hessian output argument is Hessian of f only
  // TODO : test all exitflag values
  //
  // Examples
  // // A basic case : 
  // // we provide only the objective function and the nonlinear constraint
  // // function : we let fmincon compute the gradients by numerical 
  // // derivatives.
  // function f = objfun ( x )
  //   f = exp(x(1))*(4*x(1)^2 + 2*x(2)^2 + 4*x(1)*x(2) + 2*x(2) + 1)
  // endfunction
  // function [c, ceq] = confun(x)
  //   // Nonlinear inequality constraints
  //   c = [
  //     1.5 + x(1)*x(2) - x(1) - x(2)
  //     -x(1)*x(2) - 10
  //   ]
  //   // Nonlinear equality constraints
  //   ceq = []
  // endfunction
  // // The initial guess
  // x0 = [-1,1];
  // // The expected solution : only 4 digits are guaranteed
  // xopt = [-9.547345885974547   1.047408305349257]
  // fopt = 0.023551460139148
  // // Run fmincon
  // [x,fval,exitflag,output,lambda,grad,hessian] = ..
  //   fmincon ( objfun,x0,[],[],[],[],[],[], confun )
  //
  // Examples
  // // A case where we provide the gradient of the objective 
  // // function and the Jacobian matrix of the constraints.
  // // The objective function and its gradient
  // function [f,G] = objfungrad ( x )
  //   [lhs,rhs]=argn()
  //   f = exp(x(1))*(4*x(1)^2+2*x(2)^2+4*x(1)*x(2)+2*x(2)+1)
  //   if ( lhs  > 1 ) then
  //     G = [
  //       f + exp(x(1)) * (8*x(1) + 4*x(2))
  //       exp(x(1))*(4*x(1)+4*x(2)+2)
  //     ]
  //   end
  // endfunction
  // // The nonlinear constraints and the Jacobian 
  // // matrix of the constraints
  // function [c,ceq,DC,DCeq] = confungrad(x)
  //   // Inequality constraints
  //   c(1) = 1.5 + x(1) * x(2) - x(1) - x(2)
  //   c(2) = -x(1) * x(2)-10
  //   // No nonlinear equality constraints
  //   ceq=[]
  //   [lhs,rhs]=argn()
  //   if ( lhs > 2 ) then
  //     // DC(:,i) = gradient of the i-th constraint
  //     // DC = [
  //     //   Dc1/Dx1  Dc2/Dx1
  //     //   Dc1/Dx2  Dc2/Dx2
  //     //   ]
  //     DC= [
  //     x(2)-1, -x(2)
  //     x(1)-1, -x(1)
  //     ]
  //     DCeq = []
  //   end
  // endfunction
  // // Test with both gradient of objective and gradient of constraints
  // options = optimset("GradObj","on","GradConstr","on");
  // // The initial guess
  // x0 = [-1,1];
  // // The expected solution : only 4 digits are guaranteed
  // xopt = [-9.547345885974547   1.047408305349257]
  // fopt = 0.023551460139148
  // // Run fmincon
  // [x,fval,exitflag,output] = ..
  //   fmincon(objfungrad,x0,[],[],[],[],[],[], confungrad,options)
  //
  // Examples
  // // A case where we set the bounds of the optimization.
  // // By default, the bounds are set to infinity.
  // function f = objfun ( x )
  //   f = exp(x(1))*(4*x(1)^2 + 2*x(2)^2 + 4*x(1)*x(2) + 2*x(2) + 1)
  // endfunction
  // function [c, ceq] = confun(x)
  //   // Nonlinear inequality constraints
  //   c = [
  //     1.5 + x(1)*x(2) - x(1) - x(2)
  //     -x(1)*x(2) - 10
  //   ]
  //   // Nonlinear equality constraints
  //   ceq = []
  // endfunction
  // // The initial guess
  // x0 = [-1,1];
  // // The expected solution
  // xopt = [0   1.5]
  // fopt = 8.5
  // // Make sure that x(1)>=0, and x(2)>=0
  // lb = [0,0];
  // ub = [ ];
  // // Run fmincon
  // [x,fval] = fmincon ( objfun , x0,[],[],[],[],lb,ub,confun)
  //
  // Authors
  // Michael Baudin, DIGITEO, 2010
  // Stephane Mottelet, 2020

  [lhs,rhs]=argn()
  apifun_checkrhs ( "fmincon" , rhs , [1 2 4 6 8 9 10] )
  apifun_checklhs ( "fmincon" , lhs , 1 : 7 )
  //
  if rhs == 1 && typeof(varargin(1)) ==  "st"
    fmincon_problem = varargin(1);
    fields = list("objective","x0",["A","Aineq"],["b","bineq"],"Aeq","beq","lb","ub","nonlcon","options");
    fmincon_args = list();
    for i = 1:length(fields)
        k = find(isfield(fmincon_problem,fields(i)));
        if isempty(k)
            if fields(i) <> "options"
                fmincon_args(i) = [];
            end
        else
            fmincon_args(i) = fmincon_problem(fields(i)(k))
        end        
    end
    rhs = length(fmincon_args);
  else
    fmincon_args = varargin;  
  end
  //
  fun = fmincon_args(1)
  x0 = fmincon_args(2)
  if ( rhs<3 ) then
    A = []
    b = []
  else
    A = fmincon_args(3)
    b = fmincon_args(4)
  end
  if ( rhs<5 ) then
    Aeq = []
    beq = []
  else
    Aeq = fmincon_args(5)
    beq = fmincon_args(6)
  end
  if ( rhs<7 ) then
    lb = []
    ub = []
  else
    lb = fmincon_args(7)
    ub = fmincon_args(8)
  end
  if ( rhs<9 ) then
    nonlcon = []
  else
    nonlcon = fmincon_args(9)
  end
  if ( rhs<10) then
    options =  optimoptions("fmincon")
  else
    options = fmincon_args(10)
  end
  //
  // Check the types
  apifun_checktype ( "fmincon" , fun ,     "fun" ,      1 , ["function" "list"])
  apifun_checktype ( "fmincon" , x0 ,      "x0" ,       2 , "constant" )
  apifun_checktype ( "fmincon" , A ,       "A" ,        3 , ["constant" "sparse"] )
  apifun_checktype ( "fmincon" , b ,       "b" ,        4 , "constant" )
  apifun_checktype ( "fmincon" , Aeq ,     "Aeq" ,      5 , ["constant" "sparse"] )
  apifun_checktype ( "fmincon" , beq ,     "beq" ,      6 , "constant" )
  apifun_checktype ( "fmincon" , lb ,      "lb" ,       7 , "constant" )
  apifun_checktype ( "fmincon" , ub ,      "ub" ,       8 , "constant" )
  apifun_checktype ( "fmincon" , nonlcon , "nonlcon" ,  9 , ["constant" "function" "list"] )
  apifun_checktype ( "fmincon" , options , "options" , 10 , "optim_options_fmincon" )
  //
  // Get the number of variables, number of linear inequalities, number of linear equalities
  nbvar = size(x0,"*")
  nA = size(A,"r")
  nAeq = size(Aeq,"r")
  //
  // Cross check the input arguments
  //===================================================
  // Get x0
  apifun_checkvector ( "fmincon" , x0 , "x0" , 2 , nbvar )
  //===================================================
  // Cross-check the linear inequality constraints A and b
  if ( A <> [] & b == [] ) then
    errmsg = msprintf(gettext("%s: The matrix A is not empty, but the column vector b is empty."), "fmincon", rhs)
    error(errmsg)
  end
  if ( A == [] & b <> [] ) then
    errmsg = msprintf(gettext("%s: The matrix A is empty, but the column vector b is not empty."), "fmincon", rhs)
    error(errmsg)
  end
  if ( A <> [] ) then
    apifun_checkdims ( "fmincon" , A , "A" , 3 , [nA nbvar] )
    apifun_checkvector ( "fmincon" , b , "b" , 4 , nA )
    // Change b into a column vector
    b = b(:)
  end
  // Cross-check the linear equality constraints Aeq and beq
  if ( Aeq <> [] & beq == [] ) then
    errmsg = msprintf(gettext("%s: The matrix Aeq is not empty, but the column vector beq is empty."), "fmincon", rhs)
    error(errmsg)
  end
  if ( Aeq == [] & beq <> [] ) then
    errmsg = msprintf(gettext("%s: The matrix Aeq is empty, but the column vector beq is not empty."), "fmincon", rhs)
    error(errmsg)
  end
  if ( Aeq <> [] ) then
    apifun_checkdims ( "fmincon" , Aeq , "Aeq" , 5 , [nAeq nbvar] )
    apifun_checkvector ( "fmincon" , beq , "beq" , 6 , nAeq )
    // Change beq into a column vector
    beq = beq(:)
  end
  // Cross-check the upper and lower bounds lb and ub
  if ( lb <> [] ) then
    apifun_checkvector ( "fmincon" , lb , "lb" , 7 , nbvar )
    // Change lb into a column vector
    lb = lb(:)
  end
  if ( ub <> [] ) then
    apifun_checkvector ( "fmincon" , ub , "ub" , 8 , nbvar )
    // Change ub into a column vector
    ub = ub(:)
  end
  // Cross-check the options
  //apifun_checkscalar ( "fmincon" , options , "options" , 10 )
  //
  // Run the algorithm
  algorithm = options.algorithm;
  // Compute the function name from the algorithm name
  solver_name = "fmincon_"+algorithm
  // Check that the function is defined
  if ( isdef(solver_name) == [] ) then
    errmsg = msprintf(gettext("%s: Unexpected algorithm %s."), "fmincon", algorithm)
    error(errmsg)
  end
  // Launch the solver
  solver = evstr(solver_name);
  
  [x,fval,exitflag,output,lambda,grad,hessian] = solver( fun,x0,A,b,Aeq,beq,lb,ub,nonlcon,options );
  x = matrix(x,size(x0));
endfunction


