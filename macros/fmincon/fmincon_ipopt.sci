//
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2020-2023 - UTC - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source w is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function [x,fval,exitflag,output,lambda,grad,hessian] = fmincon_ipopt (fun,x0,A,b,Aeq,beq,lb,ub,nonlcon,options)
    
    // Uses IPOPT as an implementation of fmincon.
    //
    // Calling Sequence
    //   x = fmincon_ipopt(fun,x0,A,b)
    //   x = fmincon_ipopt(fun,x0,A,b,Aeq,beq)
    //   x = fmincon_ipopt(fun,x0,A,b,Aeq,beq,lb,ub)
    //   x = fmincon_ipopt(fun,x0,A,b,Aeq,beq,lb,ub,nonlcon)
    //   x = fmincon_ipopt(fun,x0,A,b,Aeq,beq,lb,ub,nonlcon,options)
    //   [x,fval,exitflag,output,lambda,grad,hessian] = fmincon_ipopt ( ... )
    //
    // Parameters
    //   fun: a function, the function to minimize. See below for the complete specifications.
    //   x0: a nx1 or 1xn matrix of doubles, where n is the number of variables. The initial guess for the optimization algorithm.
    //   A: a nil x n matrix of doubles, where n is the number of variables and nil is the number of linear inequalities.
    //   b: a nil x 1 matrix of doubles, where nil is the number of linear inequalities.
    //   Aeq: a nel x n matrix of doubles, where n is the number of variables and nel is the number of linear equalities.
    //   beq: a nel x 1 matrix of doubles, where nel is the number of linear inequalities.
    //   lb: a nx1 or 1xn matrix of doubles, where n is the number of variables. The lower bound for x.
    //   ub: a nx1 or 1xn matrix of doubles, where n is the number of variables. The upper bound for x.
    //   nonlcon: a function, the nonlinear constraints. See below for the complete specifications.
    //   options: an optional struct, as provided by optimset
    //
    // Description
    //   We assume that the arguments of fmincon_ipopt have already been checked by fmincon.
    //   This function is not designed to be available directly to the end-user.
    //
    // Authors
    // Michael Baudin, DIGITEO, 2010
    // Stéphane Mottelet, 2020
    //
    //===================================================
    // Extract optional arguments for objective and/or constraints
    if typeof(fun) == "list"
        if size(fun) == 0 || typeof(fun(1)) <> "function"
            error(msprintf(_("%s: first element of parameter %d should be a function.\n"),"fmincon", 1));
        end
    elseif typeof(fun) == "function"
        fun = list(fun);
    end
    
    if typeof(nonlcon) == "list"
        if size(nonlcon) == 0 || typeof(nonlcon(1)) <> "function"
            error(msprintf(_("%s: first element of parameter %d should be a function.\n"),"fmincon", 9));
        end
    elseif typeof(nonlcon) == "function"
        nonlcon = list(nonlcon);
    end
    
    if typeof(options.HessianFcn) == "list"
        hessian = options.HessianFcn;
    elseif typeof(options.HessianFcn) == "function"
        hessian = list(options.HessianFcn);
    elseif typeof(options.HessianFcn) == "string" && options.HessianFcn == "objective"
        if options.SpecifyObjectiveGradient == %f
            error(msprintf(_("%s: option HessianFcn=""objective"" needs SpecifyObjectiveGradient=true.\n"),"fmincon"));
        end
        if typeof(nonlcon) == "list"
            error(msprintf(_("%s: option HessianFcn=""objective"" is incompatible with non-linear constraints.\n"),"fmincon"));
        end
        hessian = fun;
    end

    //===================================================
    // Configure options of ipopt from user's options
    // Compute the number of constraints
    nbvar = size(x0,"*");
    nA = size(A,"r");
    nAeq = size(Aeq,"r")
    if (typeof(nonlcon) <> "list") then
        nc = 0;
        nceq = 0;
    else
        [c, ceq] = nonlcon(1)(x0, nonlcon(2:$))
        if (isempty(c)) then
            nc = 0;
        else
            nc = size(c,"r");
            if (size(c,"c") <> 1) then
                errmsg = msprintf(gettext("%s: Wrong number of columns for the variable c at output of nonlcon: expected 1, but got %d instead."),"fmincon", size(c,"c") );
                error(errmsg);
            end
        end
        if (isempty(ceq)) then
            nceq = 0;
        else
            nceq = size(ceq,"r");
            if (size(ceq,"c") <> 1) then
                errmsg = msprintf(gettext("%s: Wrong number of columns for the variable ceq at output of nonlcon: expected 1, but got %d instead."),"fmincon", size(ceq,"c") );
                error(errmsg);
            end
        end
    end
    nb_constr = nc + nceq + nA + nAeq;
    // Store the rhs/lhs of the constraints
    k = 1
    // Store rhs/lhs of the nonlinear inequality constraints
    constr_lhs(k:k+nc-1) = - %inf * ones(nc,1);
    constr_rhs(k:k+nc-1) = zeros(nc,1);
    k = k + nc;
    // Store rhs/lhs of the nonlinear equality constraints
    constr_lhs(k:k+nceq-1) = zeros(nceq,1);
    constr_rhs(k:k+nceq-1) = zeros(nceq,1);
    k = k + nceq;
    // Store rhs/lhs of the linear inequality constraints
    constr_lhs(k:k+nA-1) = - %inf * ones(nA,1);
    constr_rhs(k:k+nA-1) = b;
    k = k + nA;
    // Store rhs/lhs of the linear equality constraints
    constr_lhs(k:k+nAeq-1) = beq;
    constr_rhs(k:k+nAeq-1) = beq;
    k = k + nAeq;
    // Set type of each variable: 
    // I do not have the necessary information from the input arguments
    // I set all variable to nonlinear = 1
    var_lin_type = ones(nbvar,1);
    // Set the type of each constraint
    // nonlinear = 1
    // linear = 0
    constr_lin_type = [];
    k = 1;
    // Store type of the nonlinear inequality constraints
    constr_lin_type(k:k+nc-1) = ones(nc,1);
    k = k + nc;
    // Store type of the nonlinear equality constraints
    constr_lin_type(k:k+nceq-1) = ones(nceq,1);
    k = k + nceq;
    // Store type of the linear inequality constraints
    constr_lin_type(k:k+nA-1) = zeros(nA,1);
    k = k + nA;
    // Store type of the linear inequality constraints
    constr_lin_type(k:k+nAeq-1) = zeros(nAeq,1);
    k = k + nAeq;
    //===================================================
    // Configure special settings for IPOPT
    //===================================================
    // Init ipopt structures
    //
   ipopt_problem = struct();
   ipopt_problem.jacobian_approximation = "finite-difference-values";
    //
    // Convergence
   ipopt_problem.tol = options.OptimalityTolerance;
   ipopt_problem.max_iter = options.MaxIterations;
   ipopt_problem.constr_viol_tol = options.ConstraintTolerance;
   ipopt_problem.acceptable_obj_change_tol = options.FunctionTolerance;
    // Scaling
    if options.ScaleProblem then
       ipopt_problem.nlp_scaling_method = "gradient-based";
    end
    // Constraints
    if options.HonorBounds == %f then
        // bounds replaced by inequality constraint
       ipopt_problem.replace_bounds = "yes";
    end

    // barrier parameter (mu)
   ipopt_problem.mu_init = options.InitBarrierParam;
    // Derivative approximation
   ipopt_problem.findiff_perturbation = options.FiniteDifferenceStepSize;
    // Output
   ipopt_problem.print_level = 0;

    ////////
    // Configure the computation of the Hessian matrix.
    //////
    if exists("hessian","local")
       ipopt_problem.hessian_approximation = "exact";
        if hessian == fun // [f,g,H] = fun() syntax
            [f,grad,h] = fun(1)(x0,fun(2:$));
            ipopt_problem.dh = [];
        else
            dh = list(ipopt_h,nc,nceq,hessian(:));
            ipopt_problem.dh = dh;
            h = dh(1)(x0, %t, 1, rand(nb_constr,1), %t,dh(2:$));
        end
        if issparse(h)
            ipopt_problem.sparse_dh = h;
        end
    elseif options.HessianApproximation =="finite-difference"
        if options.SpecifyObjectiveGradient==%f 
            error("Finite differences Hessian approximation needs SpecifyObjectiveGradient == %t");
        elseif (nc+nceq > 0 && options.SpecifyConstraintGradient == %f)
            error("Finite differences Hessian approximation with non-linear constraints needs SpecifyConstraintGradient == %t")
        end
        if ~isempty(options.HessPattern)
           ipopt_problem.hessian_approximation = "exact";
            ipopt_problem.sparse_dh = options.HessPattern;
            // construct sciColPack engine
            if nc+nceq > 0 // there are nonlinear constraints
                compHessian = spCompHessian(grad_lagr_nonlcon,options.HessPattern);
                ipopt_problem.dh = list(ipopt_h_comp_nonlin,compHessian,fun,nonlcon);
            else // Hessian of lagrangian is Hessian of fun
                compHessian = spCompHessian(grad_lagr,options.HessPattern,FiniteDifferenceType="CENTERED")
                ipopt_problem.dh = list(ipopt_h_comp,compHessian,fun(:));
            end
        else  
            warning("Consider setting Hessian pattern if the Hessian is sparse.")
        end
    else
       ipopt_problem.hessian_approximation = "limited-memory";
        if iscell(options.HessianApproximation) then
           ipopt_problem.limited_memory_max_history = options.HessianApproximation{2};
        end
    end

    if options.CheckGradients
       ipopt_problem.derivative_test = "second-order";
        if or(typeof(options.HessianFcn)==["function","list"])
           ipopt_problem.derivative_test = "second-order";
        end
       ipopt_problem.print_level = 4;
       ipopt_problem.derivative_test_print_all = "yes";
       ipopt_problem.jacobian_approximation = "exact";
    end
    //===================================================
    // Callback
    if options.Display == "iter" || ...
        typeof(options.PlotFcn) == "function" || ...
        typeof(options.PlotFcn) == "list" || ...
        typeof(options.OutputFcn) == "function" || ...
        typeof(options.OutputFcn) == "list"
        //
        ipopt_problem.int_cb = list(ipopt_callback,options);
    end
    //===================================================
    // Callback at Initialization, if applicable
    //
    if isfield(ipopt_problem,"int_cb")
        fval = ipopt_f(x0,%t,fun(:));
        cb_ipopt_params = struct("x",x0,"obj_count",1,"obj_value",fval,"iter",0);
        out = ipopt_output_callback(cb_ipopt_params,"init",options.PlotFcn,options.OutputFcn);
    end
    //===================================================
    // Prepare Ipopt call
    //
    ipopt_problem.x0 = x0;
    ipopt_problem.constr_lhs = constr_lhs; 
    ipopt_problem.constr_rhs = constr_rhs;
    ipopt_problem.constr_lin_type = constr_lin_type;
    ipopt_problem.var_lin_type = var_lin_type;
    ipopt_problem.x_upper = ub;
    ipopt_problem.x_lower = lb;
    
    //
    // Choose setup w.r.t. linearity and provided derivatives
    // of objective and constraints
    //
    if options.SpecifyObjectiveGradient && exists("hessian","local") && hessian == fun
        ipopt_problem.f = list(ipopt_f_df_dh, fun(:));
    elseif options.SpecifyObjectiveGradient
        ipopt_problem.f = list(ipopt_f_df, fun(:));
    elseif options.FiniteDifferenceType == "complexStep"
        ipopt_problem.f = list(ipopt_f_cplxstp, fun(:));
    else
        ipopt_problem.f = list(ipopt_f, fun(:));    
    end

    if options.SpecifyConstraintGradient
        ipopt_problem.g = list(ipopt_g_dg, A, Aeq, nonlcon(:));
       ipopt_problem.jacobian_approximation = "exact";
        [c, ceq, dc, dceq] = nonlcon(1)(x0, nonlcon(2:$));
        if issparse(dc) || issparse(dceq)
            if (nA>0 && ~issparse(A)) || (nAeq>0 && ~issparse(Aeq))
                error("When dc and/or dceq is sparse, A and Aeq must also be sparse.");
            end
            // Jacobian pattern
           ipopt_problem.jacobian_approximation = "exact";
            ipopt_problem.sparse_dg = sparse([dc'; dceq'; A; Aeq]);
        end     
    elseif nc+nceq+nA+nAeq > 0 && nc+nceq == 0
        ipopt_problem.g = list(ipopt_g_lin, A, Aeq);
       ipopt_problem.jac_c_constant = "yes"; 
       ipopt_problem.jac_d_constant = "yes"; 
       ipopt_problem.jacobian_approximation = "exact";
        
        if (issparse(A) || isempty(A)) && (issparse(Aeq) || isempty(Aeq))
            ipopt_problem.sparse_dg = [A;Aeq];
        end
        
    elseif nc+nceq+nA+nAeq > 0
        ipopt_problem.g = list(ipopt_g, A, Aeq, nonlcon(:));
    end
    
    //
    [x, fval, extra] = ipopt(ipopt_problem);
    //  
    //===================================================
    // Callback at termination, if applicable
    //
    if isfield(ipopt_problem,"int_cb")
        cb_ipopt_params = struct("x",x,"obj_count",extra.fobj_eval,"obj_value",fval,"iter",extra.it_count);
        out = ipopt_output_callback(cb_ipopt_params,"done",options.PlotFcn,options.OutputFcn);
    end
    //===================================================
    // Set output arguments
    //
    select extra.status
    case 0 then
        exitflag = 1; //Optimal Solution Found
    case 1 then
        exitflag = 0; //Maximum Number of Iterations Exceeded
    case 3 then
        exitflag = 2; //Stop at Tiny Step
    case 5 then
        exitflag = -2//No feasible point was found.
    case 6 then
        exitflag = -1//Stopped by an output function or plot function.
    case 8 then
        exitflag = -3//Iterates diverging; ipopt_problem might be unbounded
    else
        exitflag = %nan;
    end
    //
    output = struct("firstorderopt",extra.kkt_error,...
    "iterations",extra.it_count,...
    "funcCount",extra.fobj_eval,...
    "constrviolation",extra.constr_viol,...
    "algorithm","ipopt",...
    "message",extra.message);
    //
    lambda = struct("lower",extra.zL,...
    "upper",extra.zU,...
    "ineqlin", extra.lambda(nc+nceq+1:nc+nceq+nA),...
    "eqlin", extra.lambda(nc+nceq+nA+1:$),...
    "ineqnonlin",extra.lambda(1:nc),...
    "eqnonlin",extra.lambda(nc+1:nc+nceq))
    //
    // final gradient
    [fval,grad] = ipopt_problem.f(1)(x,%f,ipopt_problem.f(2:$))
    //
    // reshape x0, if applicable
    x = matrix(x,size(x0));
    // FIXME : which Hessian ? Objective, Lagrangian ?
    hessian = [];
    //
    if or(options.Display == ["iter","final"]) then
        mprintf("\nTermination message: %s\n",extra.message)
    end
endfunction
//
// ipopt_f --
//  Evaluates the objective function and its gradient by finite differences (if requested)
function varargout = ipopt_f(x, new_x, _fun, varargin)
    if argn(1) == 2 // f and df requested
        if size(varargin) == 0
            varargout(2) = numderivative(_fun, x)';
        else
            varargout(2) = numderivative(list(_fun,varargin(:)), x)';
        end
    end
    varargout(1) = _fun(x, varargin(:));    
endfunction
//
//  Evaluates the objective function and its gradient by complex step
function varargout = ipopt_f_cplxstp(x, new_x, _fun, varargin)
    if argn(1) == 2 // f and df requested
        n = length(x)    
        df = zeros(n,1);
        xi=x;
        dx = 1e-100;
        for k=1:n
            xi(k)=complex(x(k),dx);
            df(k) = imag(_fun(xi,varargin(:)))/dx;
            xi(k)=x(k);
        end
        varargout(2) = df;
    end
    varargout(1) = _fun(x, varargin(:));    
endfunction

// ipopt_f_df --
//   Evaluates the objective function and its gradient (if requested)
function [f,df] = ipopt_f_df(x, new_x, _fun, varargin)
    if argn(1) == 2 // f and df requested
        [f,df] = _fun(x, varargin(:))
    else
        f = _fun(x, varargin(:));    
    end
endfunction
// ipopt_f_df_dh --
//   Evaluates the objective function its gradient and Hessian (if requested)
function [f,df,dh] = ipopt_f_df_dh(x, new_x, _fun, varargin)
    if argn(1) == 1
        f = _fun(x, varargin(:))
    elseif argn(1) == 2 // f and df requested
        [f,df] = _fun(x, varargin(:))
    else
        [f,df,dh] = _fun(x, varargin(:))
    end
endfunction
//
// ipopt_g --^
//   Evaluates the constraints.
function g = ipopt_g(x, new_x, A, Aeq, _nonlcon, varargin)
    [c,ceq] = _nonlcon(x, varargin(:));
    g = [c; ceq; A*x; Aeq*x];
endfunction
//
// ipopt_g_dg --
//   Evaluates the constraints + derivative of the constraints
function [g,dg] = ipopt_g_dg (x, new_x, A, Aeq, _nonlcon, varargin)
    [c,ceq,dc,dceq] = _nonlcon(x, varargin(:));
    // Warning: Matlab convention is that dc and dceq are gradients of constraints
    // functions hence transpose of Jacobians
    g  = [c; ceq; A*x; Aeq*x];
    dg = [dc'; dceq'; A; Aeq];
endfunction
//
// ipopt_g_dg --
//   Evaluates the linear constraints + Jacobian
function [g,dg] = ipopt_g_lin(x, new_x, A,Aeq)
    g  = [A*x; Aeq*x];
    dg = [A; Aeq];
endfunction
//
// ipopt_h --
//   Evaluates the Hessian of Lagrangian
//
function h = ipopt_h(x, new_x, objfactor, lambda_in, new_lambda, nc, nceq, _hessian, varargin)
    // return  objfactor*\nabla^2 f(x) + \sum_i lambda(i)*\nabla^2 g_i(x)
    // Matlab style hessian function has function h = hess(x,lambda), hence we use a dirty trick
    // if objfactor == 0 (used by ipopt to check hessian of constraints only)
    if objfactor == 0
        fact = 1e100;
    else
        fact = 1/objfactor;
    end
    // create only nonlinear contraints multipliers
    k=1;
    lambda.ineqnonlin = lambda_in(k:k+nc-1)*fact;
    k=k+nc;
    lambda.eqnonlin = lambda_in(k:k+nceq-1)*fact;
    k=k+nceq;
    
    h = _hessian(x,lambda,varargin(:))/fact;
endfunction
//
// grad_lagr_nonlcon --
//   Evaluates the gradient of the Lagrangian (without linear constraints contributions)
//   and when there are nonlinear constraints
function out = grad_lagr_nonlcon(x,lambda,_fun,_nonlcon)
    [obj,grad] = _fun(1)(x,_fun(2:$));
    [c,ceq,gc,gceq] = _nonlcon(1)(x,_nonlcon(2:$));
    out = grad+gc*lambda.ineqnonlin+gceq*lambda.eqnonlin;
endfunction
//
// grad_lagr --
//   Evaluates the gradient of the Lagrangian (without linear constraints contributions)
//   and when there are no nonlinear constraints
function grad = grad_lagr(x,_fun,varargin)
    [obj,grad] = _fun(x,varargin(:));
endfunction
//
// ipopt_h_comp_nonlin --
//   Evaluates the compressed Hessian of Lagrangian when there are nonlinear constraints
//0
function h = ipopt_h_comp_nonlin(x, new_x, objfactor, lambda_in, new_lambda,engine,fun,nonlcon)
    // return  objfactor*\nabla^2 f(x) + \sum_i lambda(i)*\nabla^2 g_i(x)
    // Matlab style hessian function has function h = hess(x,lambda), hence we use a dirty trick
    // if objfactor == 0 (used by ipopt to check hessian of constraints only)
    if objfactor == 0
        fact = 1e100;
    else
        fact = 1/objfactor;
    end
    // create only nonlinear contraints multipliers
    k=1;
    lambda.ineqnonlin = lambda_in(k:k+nc-1)*fact;
    k=k+nc;
    lambda.eqnonlin = lambda_in(k:k+nceq-1)*fact;
    k=k+nceq;
    h = engine(x,lambda,fun,nonlcon)/fact;
endfunction
//
// ipopt_h_comp --
//   Evaluates the compressed Hessian of Lagrangian when there are no nonlinear constraints
//
function h = ipopt_h_comp(x, new_x, objfactor, lambda_in, new_lambda,engine,varargin)
    // return  objfactor*\nabla^2 f(x) 
    h = engine(x,varargin(:))*objfactor;
endfunction
// ipopt_callback --
//   general callback function
function out = ipopt_callback(ipopt_params, options)
    out = %t;
    if options.Display == "iter"
        if modulo(ipopt_params.iter,20) == 0
            mprintf(" Iter  Func-count  Function value   Feasibility   Step Length     Step Norm   First-order\n");
            mprintf("                                                                               optimality\n");
        end
        mprintf("%5d  %10d  %14.6e   %11.4e   %11.4e   %11.4e   %11.4e\n",...
       ipopt_params.iter,ipopt_params.obj_count,ipopt_params.obj_value, ...
       ipopt_params.inf_pr,ipopt_params.alpha_pr,ipopt_params.d_norm, ...
       ipopt_params.curr_nlp_error);
    end
    if isfield(ipopt_params,"x")
        out = ipopt_output_callback(ipopt_params,"iter",options.PlotFcn,options.OutputFcn)
    end    
endfunction
//
// ipopt_output_callback --
//   output type (display, plot, ...) callbacks
function out = ipopt_output_callback(ipopt_params,state,varargin)
    out = %t;
    optimValues = struct("funccount",ipopt_params.obj_count,"fval",ipopt_params.obj_value,"iteration",ipopt_params.iter);
    for i=1:size(varargin)
        fun = varargin(i)
        try
            if typeof(fun) == "function"
                if i==1
                    fun(ipopt_params.x,optimValues,state)
                else
                    out = ~fun(ipopt_params.x,optimValues,state)
                end                
            elseif typeof(fun) == "list"
                if i==1
                    fun(1)(ipopt_params.x,optimValues,state,fun(2:$))
                else
                    out = ~fun(1)(ipopt_params.x,optimValues,state,fun(2:$))
                end                
            end
            if out == %f
                return
            end
        catch
            error(lasterror())
        end
    end
end

























